# Python Flex Net Sim

This simulator is a Python remake of [Flex Net Sim](https://gitlab.com/DaniloBorquez/flex-net-sim) which was done in C++.

## Objective

We understand that Python is an accessible language, so to facilitate the work of researchers we are developing this version focused on the accessibility of the simulator.

## Installation

You can install it using the pip command:

```
python -m pip install flexnetsim
```

The library needs a C++ compiler because depends on Cython Python Module. You can get more information Here:

[Installing Cython](https://cython.readthedocs.io/en/latest/src/quickstart/install.html)

## Collaborators

- Gonzalo España
- Danilo Borquéz
- Cristian Véliz
- Felipe Falcón

## License

[MIT](https://choosealicense.com/licenses/mit/)
