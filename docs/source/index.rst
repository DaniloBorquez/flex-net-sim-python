.. Flex Net Sim Python documentation master file, created by
   sphinx-quickstart on Wed Dec  1 20:55:48 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Flex Net Sim documentation!
===============================================

This simulator is a Python remake of `Flex Net Sim <https://gitlab.com/DaniloBorquez/flex-net-sim>`_ which was done in C++.

We understand that Python is an accessible language, so to facilitate the work of researchers we are developing this version focused on the accessibility of the simulator.

.. note::

   This project is under active development.

.. toctree::
   :maxdepth: 2
   :caption: Classes:

   modules
