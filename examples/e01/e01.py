from flexnetsim import *
import os


def first_fit_algorithm(src: int, dst: int, b: Bitrate, c: Connection, n: Network, path):
    numberOfSlots = b.get_number_of_slots(0)
    link_ids = path[src][dst][0]
    general_link = []
    for _ in range(n.get_link(0).slots_number()):
        general_link.append(False)
    for link in link_ids:
        link = n.get_link(link)
        for slot in range(link.slots_number()):
            general_link[slot] = general_link[slot] or link.get_slot(
                slot)
    currentNumberSlots = 0
    currentSlotIndex = 0

    for j in range(len(general_link)):
        if not general_link[j]:
            currentNumberSlots += 1
        else:
            currentNumberSlots = 0
            currentSlotIndex = j + 1
        if currentNumberSlots == numberOfSlots:
            for k in link_ids:
                c.add_link(
                    k, from_slot=currentSlotIndex, to_slot=currentSlotIndex+currentNumberSlots)
            return Controller.status.ALLOCATED, c
    return Controller.status.NOT_ALLOCATED, c


absolutepath = os.path.abspath(__file__)
fileDirectory = os.path.dirname(absolutepath)

sim = Simulator(fileDirectory + "/NSFNet.json", fileDirectory + "/routes.json")

sim.goalConnections = 1e5

sim.set_allocation_algorithm(first_fit_algorithm)

sim.init()
sim.run()
